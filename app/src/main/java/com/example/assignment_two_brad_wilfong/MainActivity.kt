package com.example.assignment_two_brad_wilfong

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tip_button.setOnClickListener { handleClick() }
    }

    private fun handleClick() {
        try {
            val enteredText = input_tip.editableText.toString()
            val enteredValue = enteredText.toDouble()
            val tipTotal = when(Radio_Group.checkedRadioButtonId){
                R.id.per_ten -> (enteredValue) * (.1)
                R.id.per_twenty -> (enteredValue) * (.2)
                R.id.per_thirty -> (enteredValue) * (.3)
                else -> throw Exception()
            }
            val total = tipTotal + enteredValue

            val stringOutput = resources.getString(R.string.app_tipTotal, tipTotal, total)
            result_tv.text = stringOutput

        }
        catch(e: Exception){
            result_tv.text = resources.getString(R.string.error_string)
        }

    }
}
